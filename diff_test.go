package ipynbdiff

import (
	"os"
	"testing"
)

func TestIpynbDiff_GenerateExample(t *testing.T) {

	examplePath := os.Getenv("IPYNB_EXAMPLE")

	if examplePath == "" {
		t.Skip("No example set")
	}

	var diff string
	var err error

	basePath := "example/" + examplePath + "/"
	leftFilePath := basePath + "from.ipynb"
	rightFilePath := basePath + "to.ipynb"

	if diff, err = Diff(leftFilePath, rightFilePath, 3, true); err != nil {
		panic(err)
	}

	if err = os.WriteFile(basePath+"diff_md.txt", []byte(diff), 0644); err != nil {
		panic(err)
	}

	if diff, err = Diff(leftFilePath, rightFilePath, 3, false); err != nil {
		panic(err)
	}

	if err = os.WriteFile(basePath+"diff.txt", []byte(diff), 0644); err != nil {
		panic(err)
	}
}
