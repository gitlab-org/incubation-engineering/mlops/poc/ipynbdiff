module gitlab.com/gitlab-org/incubation-engineering/mlops/ipynbdiff

go 1.17

require (
	github.com/pmezard/go-difflib v1.0.0
	gopkg.in/yaml.v2 v2.4.0
)

require (
	github.com/kr/pretty v0.1.0 // indirect
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
)
