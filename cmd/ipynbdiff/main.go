package main

import (
	"flag"
	"fmt"
	"gitlab.com/gitlab-org/incubation-engineering/mlops/ipynbdiff"
	"log"
	"os"
)

func main() {
	var context = flag.Int("context", 3, "Context lines to be shown on the diff")
	var noMd = flag.Bool("no-md", false, "Create diffs without converting to markdown first")

	flag.Parse()

	var fromFile, toFile string

	if fromFile = flag.Arg(0); fromFile == "" {
		fmt.Println("ipynbdiff requires two file paths")
		os.Exit(1)
	}

	if toFile = flag.Arg(1); toFile == "" {
		fmt.Println("ipynbdiff requires two file paths")
		os.Exit(1)
	}

	diff, err := ipynbdiff.Diff(fromFile, toFile, *context, !*noMd)

	if err != nil {
		log.Fatal(err)
	}

	fmt.Print(diff)

}
