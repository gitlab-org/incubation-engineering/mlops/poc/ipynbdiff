package main

import (
	"bytes"
	"flag"
	"fmt"
	"log"
	"os"

	"gitlab.com/gitlab-org/incubation-engineering/mlops/ipynbdiff"
)

const helpString = `
Transforms an .ipynb file into a markdown file, stripping out most of the noise. 

USAGE:
    ipynb2md -i path/to/input [-o path/to/output] 

OPTIONS:
    - -i : Path to input file to be converted
    - -o (optional): Saves the transformation to file, rather than printing to stdout
`

func main() {

	var input = flag.String("i", "", "Path to input file")
	var output = flag.String("o", "", "Output file. If not set, will use stdout")
	var printHelp = flag.Bool("help", false, "Print Help")
	flag.Parse()

	if *printHelp {
		fmt.Print(helpString)
		os.Exit(0)
	}

	if *input == "" {
		log.Fatalln("Input argument is mandatory, run 'ipynb2md -help' for usage")
	}

	var file []byte
	var err error

	if file, err = os.ReadFile(*input); err != nil {
		log.Fatalln(err)
	}

	var converted bytes.Buffer

	if converted, err = ipynbdiff.Convert(file); err != nil {
		log.Fatalln(err)
	}

	outputTarget := os.Stdout

	if *output != "" {
		if outputTarget, err = os.Create(*output); err != nil {
			log.Fatalln(err)
		}
	}

	if _, err = fmt.Fprint(outputTarget, converted.String()); err != nil {
		log.Fatalln(err)
	}
}
